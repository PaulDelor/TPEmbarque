***TP Système embarqué - Paul DELOR-BORIES***

Le but du tp est de faire clignoter la led présente sur l'Arduino Uno à intervalles réguliers.

Le projet à été crée via PlatformIO, sur VSCodium, dans une VM Linux.

Pour créer le projet, il suffit d'aller sur le menu Home de PlatformIo, trouvable dans la barre bleu présente en bas de VSCodium, et de sélectionner "+ New Project". Ensuite, il faut donner un nom au projet, seléctionner le matériel que nous allons utiliser (ici Arduino Uno), et notre framework (ici Arduino).

Le projet est ensuite créé avec les fichiers et librairies nécessaires à notre travail, il suffit désormais de coder dans main.cpp.

Les étapes suivantes décrivent le travail effectué pendant ce TP, et correspondent aux commits présents sur le dépôt Git.

**Etape 1: On utilise les fonctions arduino pour faire clignoter la led à intervalle régulier**

Arduino fourni des fonctions facile d'utilisation pour gérer les différentes fonctionnalités présentes sur leurs cartes. Ces fonctions sont trouvables au lien suivant : https://www.arduino.cc/reference/en/

Ici, nous avons utilisé pinMode(), qui permet de paramétrer la Pin 13 en sortie. La Pin 13 correspond à la led présente sur la carte, et la mettre en sortie nous permet de controller si elle est allumée ou non.

Le contrôle de l'état de la led se fait ensuite via la fonction digitalWrite(). HIGH correspond à la led allumée, et LOW à la led éteinte.

Enfin, on espace les deux états de la led par delay(), qui met en pause l'éxecution du programme pendant une durée choisie, 1000 correspondant à 1 seconde de pause.

**Etape 2: On remplace la fonction digitalWrite() en écrivant directement sur le port B pour allumer et éteindre la led**

Afin de mieux comprendre comment fonctionne les fonctions fournit par Arduino, nous allons les remplacer en écrivant directement sur les Port qui affecte le comportement de la carte.

La première fonction que l'on remplace est digitalWrite(). En écrivant sur le cinquième bit du PORTB, nous pouvons changer l'état de la led.

Ceci est réallisé tout d'abord grâce à LED_MASK (que nous allons aussi utiliser pour les autres fonctions) qui décale un 1 jusqu'au bit 5.
Ensuite, la formule |= LED_MASK permet de mettre le bit 5 de PORTB à 1, allumant la led.
Enfin, la formule &= ~LED_MASK met le bit 5 de PORTB à 0.

**Etape 3: On remplace la fonction pinMode() en écrivant directement sur le port B pour paramétrer le port B en sortie**

La deuxième fonction que l'on remplace est pinMode(). Il est possible de changer l'état d'une Pin en écrivant sur sur le DDRX correspondant (ici DDRB). Toujours grâce à LED_MASK, on utilise |= pour mettre le bit 5 de DDRB à 1, et ainsi mettre la Pin 13 en sortie.

**Etape 4: On remplace les fonctions led_on/led_off() par led_toggle(), qui alterne l'état de la led en écrivant sur le port B**

Avant de remplacer delay, on explore encore le contrôle de l'état de la led. EN effet, il est possible d'alterner l'état d'une Pin en écrivant sur le PINX correspondant, ici PINB. Il nous suffit juste mettre notre LED_MASK sur PINB pour qu'à chaque appel de led_toggle(), l'état de led alterne entre allumée ou éteinte.
 
**Etape 5: On remplace la fonction delay en utilisant le timer et les interuptions de l'Arduino Uno**

l'Arduino Uno possède un timer que l'on peut configurer. Cependant, delay facilite grandement la tâche, car pour utiliser directement le timer, il faut définir sont prescaler, et le savoirs combien de ticks sont présent dans une seconde.

Une fois notre timer défini, il est possible d'utiliser une intéruption pour appeler une fonction (ici led_toggle()) à intervalles réguliers