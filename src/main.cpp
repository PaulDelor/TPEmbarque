#include <Arduino.h>

#define LED 13
#define LED_MASK (1<<5)
#define CS_PRESCALER_1024 ((1 << CS10) | (1 << CS12))
#define TICKS_IN_SECOND 15625

void led_setup() {
  DDRB |= LED_MASK;
}

void led_on() {
  PORTB|= LED_MASK;
}

void led_off() {
  PORTB &= ~LED_MASK;
}

void led_toggle() {
  PINB = LED_MASK;
}

void timer_setup() {
  TCCR1A = 0;
  TCCR1B = CS_PRESCALER_1024 | (1 << WGM12);
  OCR1A = TICKS_IN_SECOND;
  TIMSK1 = (1 << OCIE1A);
}

ISR(TIMER1_COMPA_vect) {
  led_toggle();
}

void setup() {
  Serial.begin(9600);
  timer_setup();
  led_setup();
  Serial.println("Begin");
}

void loop() {
}